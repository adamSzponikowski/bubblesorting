import javax.swing.*;
import java.util.ArrayList;
import java.util.Random;

class BubbleSortGUI {
    public static void main(String[] args) {
        int x = 0;
        ArrayList<Integer> list = new ArrayList<>(30);
        Random generator = new Random();
        while (x < 30) {
            list.add((Integer) generator.nextInt(30));
            x++;
        }

        JFrame frame = new JFrame("Bubble Sort");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400, 400);

        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        JTextArea beforeSort = new JTextArea(5, 20);
        beforeSort.setEditable(false);
        beforeSort.append("Before sorting:\n");
        for (Integer integer : list) {
            beforeSort.append(integer.toString() + " ");
        }

        JTextArea afterSort = new JTextArea(5, 20);
        afterSort.setEditable(false);

        JButton sortButton = new JButton("Sort");
        sortButton.addActionListener(e -> {
            for (int i = 0; i < list.size(); i++) {
                for (int j = i + 1; j < list.size(); j++) {
                    if (list.get(i) > list.get(j)) {
                        int temp = list.get(i);
                        list.set(i, list.get(j));
                        list.set(j, temp);
                    }
                }
            }
            afterSort.append("After sorting:\n");
            for (Integer integer : list) {
                afterSort.append(integer.toString() + " ");
            }
        });

        panel.add(beforeSort);
        panel.add(sortButton);
        panel.add(afterSort);

        frame.add(panel);

        frame.setVisible(true);
    }
}
